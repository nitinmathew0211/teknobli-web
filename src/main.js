// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueCarousel from 'vue-carousel'
import '../node_modules/bootstrap/dist/js/bootstrap.min.js'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import firebase from 'firebase';

Vue.use(VueCarousel)
Vue.config.productionTip = false

const config = {
  apiKey: "AIzaSyAR7H_9Y3SKLlYZnXKfwLKnupPYE3MLr3I",
    authDomain: "test-1d656.firebaseapp.com",
    databaseURL: "https://test-1d656.firebaseio.com",
    projectId: "test-1d656",
    storageBucket: "test-1d656.appspot.com",
    messagingSenderId: "986872785161"
};
firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    /* eslint-disable no-new */
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount('#app');
  }
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
