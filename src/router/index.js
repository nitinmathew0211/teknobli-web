import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import Category from '@/pages/Category'
import Cart from '@/pages/Cart'
import Product from '@/pages/Product'
import Thankyou from '@/pages/Thankyou'
import Checkout from '@/pages/Checkout'
import Myorders from '@/pages/Myorders'
import SearchProduct from '@/pages/SearchProduct'
import ProductRating from '@/pages/ProductRating'
import MyordersDetails from '@/pages/MyordersDetails'
import MerchantRating from '@/pages/MerchantRating'
import Profile from '@/pages/Profile'
import Wishlist from '@/pages/Wishlist'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/category/:categoryId',
      name: 'Category',
      component: Category
    },
    {
      path: '/product/:productId',
      name: 'Product',
      component: Product
    },
    {
      path: '/cart',
      name: 'Cart',
      component: Cart
    },
    {
      path: '/thankyou/:orderId',
      name: 'Thankyou',
      component: Thankyou
    },
    {
      path: '/checkout',
      name: 'Checkout',
      component: Checkout
    },
    {
      path: '/myorders',
      name: 'Myorders',
      component: Myorders
    },
    {
      path: '/searchresult/:string',
      name: 'SearchProduct',
      component: SearchProduct
    },{
      path: '/myordersdetails/:orderId',
      name: 'MyordersDetails',
      component: MyordersDetails
    },
    {
      path: '/productrating',
      name: 'ProductRating',
      component: ProductRating
    },
    {
      path: '/merchantrating',
      name: 'MerchantRating',
      component: MerchantRating
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/wishlist',
      name: 'Wishlist',
      component: Wishlist
    }
  ]
})
